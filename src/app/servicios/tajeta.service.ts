import { Injectable } from '@angular/core';
import { DatosUsuario } from '../interfaces/tarjeta.interface';
@Injectable({
  providedIn: 'root'
})
export class TarjetaService {
   listUsuarios: DatosUsuario[] = [
    {titular:'Lucas Alaiaga', numeroTarjeta: '1234567891234567', fechaExpiracion: '01/20'},
    {titular:'Jaun Carlos', numeroTarjeta: '1234567891004567', fechaExpiracion: '05/21'},
    {titular:'Tomas Ruiz Diaz', numeroTarjeta: '1234567891234567', fechaExpiracion: '11/22'},
    {titular:'Maria Cortes', numeroTarjeta: '1234567891234567', fechaExpiracion: '06/25'},
  ];
  constructor() { }

  getUsuario(){
    return this.listUsuarios.slice()
  }

  eliminarUsuario(index:number){
    this.listUsuarios.splice(index,1);
  }
}
